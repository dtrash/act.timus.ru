package com.part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ove on 29.06.2014.
 */
public class t1910 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int x = Integer.parseInt(reader.readLine());
        String[] wall = reader.readLine().split(" ");
        int temp, max = 0, mid = 0;

        for (int i = 0; i+2 < x; i++) {
            temp = Integer.parseInt(wall[i]) + Integer.parseInt(wall[i+1]) + Integer.parseInt(wall[i+2]);
            if (temp>max) {
                max = temp;
                mid = i+2;
            }
        }
        System.out.println(max + " " + mid);
    }
}
