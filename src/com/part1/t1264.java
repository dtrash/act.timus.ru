package com.part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ove on 29.06.2014.
 */
public class t1264 {
    public static void main(String[] args) throws IOException {
        String[] params;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        params = reader.readLine().split(" ");
        int x = Integer.parseInt(params[0]);
        int y = Integer.parseInt(params[1]);
        int result = x * (y + 1);
        System.out.println(result);
    }
}
