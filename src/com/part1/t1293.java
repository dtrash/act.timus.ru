package com.part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;

/**
 * Created by ove on 27.06.2014.
 */
public class t1293 {
    public static void main(String[] args) throws IOException {
        int n, a, b;
        StreamTokenizer tokenizer = new StreamTokenizer(new BufferedReader(new InputStreamReader(System.in)));
        tokenizer.nextToken();
        n = (int) tokenizer.nval;
        tokenizer.nextToken();
        a = (int) tokenizer.nval;
        tokenizer.nextToken();
        b = (int) tokenizer.nval;

        System.out.println(a*b*n*2);
    }
}
