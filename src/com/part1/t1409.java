package com.part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;

/**
 * Created by ove on 29.06.2014.
 */
public class t1409 {
    public static void main(String[] args) throws IOException {
        StreamTokenizer is = new StreamTokenizer(new BufferedReader(new InputStreamReader(System.in)));
        is.nextToken();
        int x = (int) is.nval;
        is.nextToken();
        int y = (int) is.nval;

        int total = x + y  -1;
        int a = total - x;
        int b = total - y;
        System.out.printf(a + " " + b);
    }
}
