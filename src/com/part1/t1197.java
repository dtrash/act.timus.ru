package com.part1;

import java.util.Scanner;

/**
 * Created by ove on 29.06.2014.
 */
public class t1197 {
          public static void main( String[] args )    {
            int[][] accessibility = { { 2, 3, 4, 4, 4, 4, 3, 2 } ,{ 3, 4, 6, 6, 6, 6 ,4, 3 } ,{ 4, 6, 8, 8, 8, 8, 6, 4 } ,{ 4, 6, 8, 8, 8, 8, 6, 4 } ,
                    { 4, 6, 8, 8, 8, 8, 6, 4 } ,{ 4, 6, 8, 8, 8, 8, 6, 4 } ,{ 3, 4, 6, 6, 6, 6, 4, 3 } ,{ 2, 3, 4, 4, 4, 4, 3, 2 }  } ;
            Scanner in = new Scanner( System.in );
            int N = in.nextInt();
            for( int i=0 ; i<N ; i++ ) {
                String p = in.next();
                System.out.println( accessibility[p.charAt(0)-'a'][p.charAt(1)-'1'] );
            }
        }
    }

