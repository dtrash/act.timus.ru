package com.part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ove on 29.06.2014.
 */
public class t1787 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String part1 = reader.readLine();
        String[] strings1 = part1.split(" ");
        String[] part2 = reader.readLine().split(" ");

        int cars = 0;
        for (int i = 0; i < Integer.parseInt(strings1[1]); i++) {
            if (cars < 0)
                cars = 0;
            cars = cars - (Integer.parseInt(strings1[0]) - Integer.parseInt(part2[i]));
        }

        if (cars>0)
            System.out.println(cars);
        else
            System.out.println(0);
    }
}
