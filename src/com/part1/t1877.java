package com.part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ove on 27.06.2014.
 */
public class t1877 {
    public static void main(String[] args) throws IOException {
        int code1, code2;
        String result = "no";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        code1 = Integer.parseInt(reader.readLine());
        code2 = Integer.parseInt(reader.readLine());

        for (int i = 2; i < 20002; i++) {
            if (i%2 == 0){
                if (code1 == i) {
                    result = "yes";
                    break;
                }

            } else {
                if (code2 == i) {
                    result = "yes";
                    break;
                }
            }
        }
        System.out.println(result);
    }
}
