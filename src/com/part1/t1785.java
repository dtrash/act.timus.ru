package com.part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ove on 27.06.2014.
 */
public class t1785 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int x = Integer.parseInt(reader.readLine());
        String answer;

        if (x > 0 && x < 5) {
            answer = "few";
        } else if (x > 4 && x < 10) {
            answer = "several";
        } else if (x > 9 && x < 20) {
            answer = "pack";
        } else if (x > 19 && x < 50) {
            answer = "lots";
        } else if (x > 49 && x < 100) {
            answer = "horde";
        } else if (x > 99 && x < 250) {
            answer = "throng";
        } else if (x > 249 && x < 500) {
            answer = "swarm";
        } else if (x > 499 && x < 1000) {
            answer = "zounds";
        } else {
            answer = "legion";
        }
        System.out.println(answer);
    }
}
