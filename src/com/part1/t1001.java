package com.part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.util.Stack;

public class t1001 {
    public static void main(String[] args) throws IOException {
        StreamTokenizer tokenizer = new StreamTokenizer(new BufferedReader(new InputStreamReader(System.in)));
        Stack<Double> stack = new Stack<>();
        while (tokenizer.nextToken() != StreamTokenizer.TT_EOF){
            if (tokenizer.ttype == StreamTokenizer.TT_NUMBER){
                stack.push(Math.sqrt(tokenizer.nval));
            }
        }

        while (!stack.isEmpty()){
            System.out.println(stack.pop());
        }
    }
}
