package com.part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ove on 29.06.2014.
 */
public class t1880 {
    public static void main(String[] args) throws IOException {
        int x, count = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        x = Integer.parseInt(reader.readLine());
        ArrayList<String> dim1 = new ArrayList<>(x);
        dim1.addAll(Arrays.asList(reader.readLine().split(" ")));

        x = Integer.parseInt(reader.readLine());
        ArrayList<String> dim2 = new ArrayList<>(x);
        dim2.addAll(Arrays.asList(reader.readLine().split(" ")));

        x = Integer.parseInt(reader.readLine());
        ArrayList<String> dim3 = new ArrayList<>(x);
        dim3.addAll(Arrays.asList(reader.readLine().split(" ")));

        for (String s : dim1) {
            if (dim2.contains(s) && dim3.contains(s))
                count++;
        }

        System.out.println(count);
    }
}
