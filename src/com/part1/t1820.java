package com.part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;

/**
 * Created by ove on 29.06.2014.
 */
public class t1820 {
    public static void main(String[] args) throws IOException {
        StreamTokenizer is = new StreamTokenizer(new BufferedReader(new InputStreamReader(System.in)));
        is.nextToken();
        int n = (int) is.nval;
        is.nextToken();
        int k = (int) is.nval;

        if (k>n) {
            System.out.println(2);
            return;
        }

        if (2*n % k >= 1)
            System.out.println(2*n/k+1);
        else
            System.out.println(2*n/k);
    }
}
